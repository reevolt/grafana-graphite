const graphite = require('graphite');

const mqtt = require("mqtt");

const config = require("../config.json");
const graphiteClient = graphite.createClient(config.graphite.host);
const mqttClient = mqtt.connect(config.mqtt.host);

const { topic } = config.app;

mqttClient.on("connect", () => {
  mqttClient.subscribe(topic, (err) => {
    if (err) {
      console.error("Unable to subsribe to topic", JSON.stringify(err, Object.getOwnPropertyNames(err)));
      return;
    }
    console.log("sucessfully subscribed to topic");
  });
});

mqttClient.on("message", (_topic, message) => {
  const { timestamp, value, name } = JSON.parse(Buffer.from(message).toString("utf-8"));
  graphiteClient.write({ [name]: value }, timestamp, (err) => {
    if (err) { console.error(err); }
  });
});

// Stop
const stopApp = () => {
  mqttClient.end();
  process.exit(0);
};

process.on("SIGINT", stopApp);
process.on("SIGTERM", stopApp);
