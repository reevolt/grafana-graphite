const rewiremock = require("rewiremock/node");
const sinon = require("sinon");
const EventEmitter = require("events");

const statsGaugeSpy = sinon.spy();
const subscribeSpy = sinon.spy();

const fakeMqtt = new class extends EventEmitter {
  subscribe(t) {
    subscribeSpy(t);
  }
};

rewiremock.proxy("./listener", {
  "mqtt": { connect: () => fakeMqtt },
  "statsd-client": class {
    gauge(a, b) { statsGaugeSpy(a, b);}
  },
  "../config": {
    app: { topic: "topic/segment" },
    statsd: "",
    mqtt: { host: "host" },
  },
});

describe("message listener", () => {

  it("should connect to topic with pattern", () => {
    // When
    fakeMqtt.emit("connect");

    // Then
    sinon.assert.calledWith(subscribeSpy, "topic/segment/+");
  });

  it("should forward new mqtt message to statsd", () => {
    // Given
    const payload = Buffer.from("12.34");

    // When
    fakeMqtt.emit("message", "topic/segment/temp.ext", payload);

    // Then
    sinon.assert.calledWith(statsGaugeSpy, "temp.ext", 12.34);
  });
});
